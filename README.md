# ArtStationDownloader

Programa para descargar imagenes y videos de Art Station

![](Demostraci%C3%B3n/ArtStationDownloader.gif)  

# Features

- Descarga imagenes

- Descarga videos de youtube

 # Como usar

[Descargar programa:](https://gitlab.com/ARKMs/artstationdownloader/uploads/8161fb7d0499df0ec4fa2b6ba54ddf15/ArtStationDownloader.zip)

Primero abre el archivo pags.txt y pega los links de artstation con las imagenes o videos a descargar

Ejecuta ArtStationImageDownload.exe, espera que se termine de descargar y listo

