﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using VideoLibrary;

namespace ArtStationImageDownload
{
    class Program
    {
        static int contador = 1;
        static IWebDriver driver;
        static string LocalPath = "";

        static void Main(string[] args)
        {
            Console.WriteLine("Made by Arkms");
            LocalPath = AppDomain.CurrentDomain.BaseDirectory;
            Console.WriteLine("Abrimos navegador");
            //Abrimos navegador
            ChromeOptions options = new ChromeOptions();
            options.AddArgument("--incognito");
            //options.AddArgument("--no-sandbox");
            //Modo sin navegador modo visual
            options.AddArgument("headless");
            options.AddArgument("window-size=1200x600");
            driver = new ChromeDriver(options);

            //Cargamos paginas 1 por 1
            string line;
            StreamReader file = new StreamReader(LocalPath + "pags.txt");
            while ((line = file.ReadLine()) != null)
            {
                Console.WriteLine("Pagina: " + line);
                DownloadImagesAndVideos(line);
            }

            Console.WriteLine("");
            Console.WriteLine("Se descargaron " + contador + " archivos");
            Console.WriteLine("Terminado de procesar, ya puedes cerrar el programa");
            file.Close();
        }

        static void DownloadImagesAndVideos(string _path)
        {
            Console.WriteLine("Cargamos página: " + _path);
            driver.Navigate().GoToUrl(_path);

            //Descargamos imagen
            try
            {
                //OBtebemos contenedor de imagenes
                IWebElement element = driver.FindElement(By.CssSelector("project-assets"));
                //Obtenemos todas las imagenes
                System.Collections.ObjectModel.ReadOnlyCollection<IWebElement> imagenes = element.FindElements(By.TagName("img")); //artwork-image

                for (int i = 0; i < imagenes.Count; i++)
                {
                    string imgPath= imagenes[i].GetAttribute("src");
                    imgPath = imgPath.Remove(imgPath.LastIndexOf("?"));
                    DescargarArchivo(imgPath);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Algo cambio en artstation, se ocupa actualizar o prueba otro link: " + _path);
                Console.WriteLine("Error: " + e);
                Thread.Sleep(3000);
            }

            //Descargamos videos
            try
            {
                //OBtebemos contenedor de imagenes
                IWebElement element = driver.FindElement(By.CssSelector("project-assets"));
                //Obtenemos todas las imagenes
                System.Collections.ObjectModel.ReadOnlyCollection<IWebElement> videos = element.FindElements(By.TagName("iframe"));

                for (int i = 0; i < videos.Count; i++)
                {
                    string videoPath = videos[i].GetAttribute("src");
                    videoPath = videoPath.Remove(videoPath.LastIndexOf("?"));
                    DescagarVideo(videoPath);
                }


            }
            catch (Exception e)
            {
                Console.WriteLine("Algo cambio en artstation, se ocupa actualizar o prueba otro link: " + _path);
                Console.WriteLine("Error: " + e);
                Thread.Sleep(3000);
            }

        }

        static void DescargarArchivo(string _path)
        {
            byte[] data;
            string name = _path.Substring(_path.LastIndexOf("/")+1);

            //Descargamos archivo
            try
            {
                using (WebClient client = new WebClient())
                {
                    Console.WriteLine("Se intenta descargar imagen: " + _path);
                    data = client.DownloadData(_path);
                }
                //Guardamos localmente y regresamos path
                string pathLocal = LocalPath + "/imgs/"+ contador + "_" + name; //la extension
                
                File.WriteAllBytes(pathLocal, data);
                Console.WriteLine("Descarga exitosa");
                contador++;
            }
            catch
            {
                Console.WriteLine("Error al descargar imagen: " + _path);
            }
        }

        static void DescagarVideo(string _path)
        {
            //Primero validamos que sea link de youtube
            if (!_path.Contains("youtube.com"))
            {
                Console.WriteLine("No es video de youtube, no es compatible: " + _path);
                return;
            }

            Console.WriteLine("Se intenta descargar video: " + _path);
            var youTube = YouTube.Default; // starting point for YouTube actions
            var video = youTube.GetVideo(_path); // gets a Video object with info about the video
            string pathLocal = LocalPath + "/imgs/" + contador + "_" + video.FullName; //la extension
            File.WriteAllBytes(pathLocal, video.GetBytes());
            Console.WriteLine("Descarga exitosa");
            contador++;
        }
    }
}
